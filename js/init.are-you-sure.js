(function ($) {
  Drupal.behaviors.are_you_sure = {
    attach: function (context, settings) {
      $(settings.are_you_sure.selector, context).areYouSure({message: settings.are_you_sure.message});
    }
  };
})(jQuery);
